package exchangeratesapi;

import io.cucumber.java.en.Given;

public class ApiKeyStepDefinition {

    public ApiKeyStepDefinition(World world) {
        this.world = world;
    }
    private World world;

    @Given("^The apikey is (.*)$")
    public void the_key_is(String key) {
        String keyFromEnv = System.getenv("APIKEY");
        switch (key) {
            case "absent": world.apikey="";
                break;
            case "wrong": world.apikey="apikey=" + key;
                break;
            case "correct": world.apikey="apikey=" + keyFromEnv;
                break;
            default:
                throw new RuntimeException("Not implemented for key: " + key);
        }
    }


}
