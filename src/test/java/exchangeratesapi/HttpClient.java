package exchangeratesapi;

import java.io.*;
import java.util.concurrent.TimeUnit;

import okhttp3.*;

class HttpClient {

    private Response response = null;

    Response makeRequest(String url) {
        return makeRequest(url,"GET",null);
    }
    Response makeRequest(String url, String method) {
        return makeRequest(url,method,"");
    }
    Response makeRequest(String url, String method, String body) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .callTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        Request request;
        switch (method) {
            case "GET":
                request = new Request.Builder()
                        .url(url)
                        .build();
                break;
            case "POST":
                RequestBody formBody = new FormBody.Builder()
                        .add("",body)
                        .build();
                request = new Request.Builder()
                        .url(url)
                        .post(formBody)
                        .build();
                break;
            default:
                    throw new RuntimeException("Not implemented for method: " + method);
        }

        try {
            response = client.newCall(request).execute();
        } catch (IOException|NullPointerException e) {
            e.printStackTrace();
        }
        return response;
    }
}
