package exchangeratesapi;

import io.cucumber.java.en.*;
import okhttp3.*;
import org.assertj.core.api.SoftAssertions;

import java.io.IOException;
import java.util.*;
import org.json.*;
import static org.junit.jupiter.api.Assertions.*;


public class StepDefinitions {
    private String baseUrl = "https://api.apilayer.com";
    private String resource = "/exchangerates_data/latest";
    private String baseParam = "EUR";
    private String symbolsParam = "GBP";
    private HttpClient client = new HttpClient();
    private Response response = null;

    public StepDefinitions(World world) {
        this.world = world;
    }
    private World world;


    @Given("^base is (.*)$")
    public void base_is(String base) {
        baseParam = base;
    }

    @Given("^symbols is (.*)$")
    public void symbols_is(String symbols) {
        symbolsParam = symbols;
    }

    @When("I send a request to the endpoint")
    public void i_send_a_request_to_the_endpoint() {
        i_send_a_request_to_endpoint("GET", "correct");
    }

    @When("^I send a (.*) request to (.*) endpoint$")
    public void i_send_a_request_to_endpoint(String method, String endpointKind) {
        resource = (endpointKind.equals("correct")) ? resource : "/wrongResource";
        String url = baseUrl + resource + "?" + world.apikey + "&symbols=" + symbolsParam + "&base=" + baseParam;
        response = client.makeRequest(url, method);
    }

    @Then("Status code should be {int}")
    public void status_code_should_be(Integer expectedCode) {
        assertEquals(expectedCode, response.code());
    }

    @Then("Response should contain following headers with values")
    public void response_should_contain_following_headers(List<List<String>> expectedHeaders) {
        Map<String, List<String>> headers = response.headers().toMultimap();
        SoftAssertions softAssertions = new SoftAssertions();
        for (List<String> row : expectedHeaders) {
            String expectedHeader = row.get(0);
            String expectedValue = row.get(1);
            softAssertions.assertThat(headers)
                    .as("Header: \"" + expectedHeader + "\" does not exist in response headers list")
                    .containsKey(expectedHeader);
            if (headers.containsKey(expectedHeader) && !expectedValue.equals("any")) {
                String actualValue = headers.get(expectedHeader).get(0);
                softAssertions.assertThat(actualValue)
                        .as("Value of header: " + expectedHeader + " is: " + actualValue
                                + ". Expected value is: " + expectedValue)
                        .isEqualTo(expectedValue);
            }
        }
        softAssertions.assertAll();
    }

    @Then("^response body should contain \"base\" key with (.*) value$")
    public void response_body_should_contain_key_with_eur_value(String expectedBase) {
        JSONObject obj;
        String baseValue = "";
        try {
            obj = new JSONObject(response.body().string());
            baseValue = obj.getString("base");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(expectedBase, baseValue);
    }

    @Then("response body should contain following keys")
    public void response_body_should_contain_following_keys(List<String> expectedKeys) {
        SoftAssertions softAssertions = new SoftAssertions();

        String bodyString = "";
        try {
            bodyString = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean keyExists;
        for (String key : expectedKeys) {
            JSONObject obj = new JSONObject(bodyString);
            keyExists = obj.has(key);
            softAssertions.assertThat(keyExists).as("Key: \"" + key + "\" was not found in response body").isTrue();
        }
        softAssertions.assertAll();
    }
}
