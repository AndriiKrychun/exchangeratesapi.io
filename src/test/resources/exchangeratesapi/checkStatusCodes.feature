Feature: To test exchangeratesapi.io
  I want to verify HTTP Response Status Codes

  Scenario Outline: GET request with different api keys
    Given The apikey is <apikey>
    When I send a GET request to correct endpoint
    Then Status code should be <code>
    Examples:
      | apikey  | code |
      | correct | 200  |
      | absent  | 401  |
      | wrong   | 401  |


  Scenario: GET request with wrong base parameter
    Given The apikey is correct
    And base is SOMETHING_WRONG
    When I send a GET request to correct endpoint
    Then Status code should be 400


  Scenario: POST request to the endpoint
    Given The apikey is correct
    When I send a POST request to correct endpoint
    Then Status code should be 403


  Scenario: GET request to wrong URL
    Given The apikey is correct
    When I send a GET request to wrong endpoint
    Then Status code should be 404