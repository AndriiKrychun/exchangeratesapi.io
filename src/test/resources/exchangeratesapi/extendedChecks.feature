Feature: To perform extended testing of exchangeratesapi.io
  I want to check response parameters

  Scenario: Check response headers
    Given The apikey is correct
    When I send a request to the endpoint
    Then Response should contain following headers with values
      | Content-Type                 | application/json |
      | x-ratelimit-limit-month      | any              |
      | x-ratelimit-remaining-month  | any              |
      | x-ratelimit-limit-day        | any              |
      | x-ratelimit-remaining-day    | any              |


  Scenario: Check response body JSON keys
    Given The apikey is correct
    When I send a request to the endpoint
    Then response body should contain following keys
      | base      |
      | date      |
      | rates     |
      | success   |
      | timestamp |


  Scenario Outline: Check response headers
    Given The apikey is correct
    And base is <base>
    When I send a request to the endpoint
    Then response body should contain "base" key with <base> value
    Examples:
      | base  |
      | EUR   |
      | USD   |
      | BTC   |



